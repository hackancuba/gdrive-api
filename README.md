# Google Drive API

This project is small API that connects with Google Drive. It's based on Python3 + Django + REST Framework + SQlite3 (for some data storage).

## Local or development deploy

Check the [app](app) subdir which contains the app and instructions.

## Production or staging deploy

Check the [deploy](deploy) subdir which contains Docker deployment files and instructions.

## License

**GDrive API** is made by [HacKan](https://hackan.net) under GNU GPL v3.0+. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).

    Copyright (C) 2018 HacKan (https://hackan.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

